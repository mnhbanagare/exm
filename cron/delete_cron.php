<?php


/*
*
* Cron to delete the db entry which is older than 15 days
* It will run as cli to delete the feed 
*/



try 
{

    //load utility file which can load our all config
    require_once('../utility.php');

      //start iterating the node
    UT::printRequired("---------- Processing Started for deleting ----------------");

    $query = "DELETE FROM blog_feed WHERE DATE(blog_date) <= DATE_SUB(CURDATE(),INTERVAL ".UT::$_CONFIG['cron_delete_feed_no_days']." DAY)";

    $result = UT::raw_query($query);

    UT::printRequired("Status => ".$result);

    UT::printRequired("---------- Processing Ended for deleting ----------------");

    
} catch (Exception $e) 
{    
    //print the Exception for debuging or we can write to file/log
   UT::printException($e,'Error in deleting feed data');
}




//process the feed file



















?>