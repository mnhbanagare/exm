<?php


/*
*
* Cron to store the RSS feed
* It will run as cli on certain interval to extract the feed
*/



try 
{

    //load utility file which can load our all config
    require_once('../utility.php');

    //start processing of feed
    processFeed(UT::$_CONFIG['feedurl']);


    
} catch (Exception $e) 
{    
    //print the Exception for debuging or we can write to file/log
   UT::printException($e,'Error in processing feed');
}




//process the feed file

function processFeed($url){

    //read the feed for url
    $rdf = file_get_contents($url);

    //replace the dc:date to date as rdf dc:date will be ignored by simple xml, we can also use dom , for demo i have used this
    $rdf = str_replace('dc:date', 'date', $rdf); 

    $rdf = new SimpleXMLElement($rdf);

    if($rdf === false)
    {
        UT::printRequired('Failed loading RDF XML:');
        foreach(libxml_get_errors() as $error) 
        {
            UT::printRequired($error->message);
        }

        throw new Exception("Failed loading RDF XML:");
    }



    //start iterating the node
    UT::printRequired("---------- Processing Started ----------------");

    foreach($rdf->children() as $blog) 
    {
        // UT::printInfo("Link:".$blog->link);

        processFeedNode($blog);

    } 


    UT::printRequired("---------- Processing Ended ----------------");

}




function processFeedNode($node)
{

    $data = array();

    if($node->link != 'https://blog.fc2.com/' && preg_match("/[blog0-9]+[\.]*/i",$node->link))
        {
        
            preg_match('@^(?:http://)?([a-zA-z0-9]+).([blog0-9]+).([^/]+)/([a-z-0-9]+)@i',$node->link, $matches);

            if(isset($matches[1]))
            {
                $username = $matches[1];
            }

            if(isset($matches[2]))
            {
                $server = $matches[2];
                $server = str_replace('blog', '', $server);
            }

            if(isset($matches[4]))
            {
                $entry = $matches[4];
                $entry = str_replace('blog-entry-', '', $entry);
            }
        

            $data['link'] = $node->link;
            $data['title'] = $node->title;
            $data['description'] = $node->description;
            $data['blog_date'] = $node->date;
            $data['username'] = $username;
            $data['server'] = $server;
            $data['blog_entry'] = $entry;


            try 
            {
                
                //we can check write succuss full or failre from the $result , time being assume always  write is succusful
                $result = UT::insert('blog_feed',$data);

            } catch (Exception $e) 
            {
                UT::printException($e);
            }
        }

}

















?>