/* db script for blogfeed store */


CREATE DATABASE `exam` /*!40100 DEFAULT CHARACTER SET utf8 */ ;

USE exam;


CREATE TABLE `blog_feed` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(500) DEFAULT NULL,
  `title` varchar(500) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `blog_entry` bigint(11) DEFAULT NULL,
  `server` int(10) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `blog_date` datetime DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entry_server_username` (`blog_entry`,`server`,`username`) COMMENT 'create a unique index to avoid the duplicate entry and keep unique post',
  KEY `link` (`link`(333)),
  KEY `blog_entry` (`blog_entry`) COMMENT 'added index which was used in where condition',
  KEY `server` (`server`) COMMENT 'added index which was used in where condition',
  KEY `username` (`username`) COMMENT 'added index which was used in where condition',
  KEY `blog_date` (`blog_date`) COMMENT 'added index which was used in where condition'
) ENGINE=MyISAM AUTO_INCREMENT=1739 DEFAULT CHARSET=utf8;





