<?php
    
/*
*
* Simple Search and Display screen
* 
*/

error_reporting(-1);
ini_set('error_reporting', E_ALL);

//load utility file which can load our all config
require_once('utility.php');

require './libs/Smarty.class.php';
require_once 'Pager/Pager.php';

$data = array();

//check privous search cookie is there on not
if(isset($_COOKIE))
{
    if(UT::is_param_set($_COOKIE,'cblog_date')){
            $data['blog_date'] = $_COOKIE['cblog_date'];       
    }

    if(UT::is_param_set($_COOKIE,'cusername')){
        $data['username'] = $_COOKIE['cusername'];    
    }

    if(UT::is_param_set($_COOKIE,'cserver')){
        $data['server'] = $_COOKIE['cserver'];    
    }

    if(UT::is_param_set($_COOKIE,'centry')){
            $data['entry'] = $_COOKIE['centry'];       
    }
        
}


if(isset($_GET)){

    foreach ($_GET as $key => $value) {
        $cookie_name = "c".$key;

        if(UT::is_param_set($_GET,$key))
        {
            setcookie($cookie_name, $_GET[$key], time() + (86400 * 365),'/');
            $data[$key] = $_GET[$key];
        }else
        {
            unset($data[$key]);
            //unset the cookie
            setcookie($cookie_name, $_GET[$key], time() -3600,'/');
            // unset($_COOKIE[$cookie_name]);
        }
    }
    
}




$smarty = new Smarty;

$smarty->compile_check = true;
// $smarty->debugging = true;

$smarty->assign("title","Filter for blog feed");
$smarty->assign("data",$data);



//construct the query 
$where = " where ";

if(isset($data)){

    if(UT::is_param_set($data,'blog_date')){

        $where  .= " date(blog_date)  = date('".mysql_real_escape_string($data['blog_date'])."')  and ";
    }

    if(UT::is_param_set($data,'username')){
        $where  .= " username  like '%".mysql_real_escape_string($data['username'])."%'  and ";
    }

    if(UT::is_param_set($data,'server')){
        $where  .= " server  = '".mysql_real_escape_string($data['server'])."'  and ";
    }

    if(UT::is_param_set($data,'entry')){
        $where  .= " blog_entry  = '".mysql_real_escape_string($data['entry'])."'  and ";
    }
}


$where  .=  " 1=1 order by blog_date desc ";

if(isset($_GET['pageID'])){
    $page = $_GET['pageID'];
}else{
    $page = 1;
}


$query = "select SQL_CALC_FOUND_ROWS * from blog_feed  ".$where. " limit ".(($page-1)*UT::$_CONFIG['per_page']).",".($page*UT::$_CONFIG['per_page']);


//get the result    
$result = UT::select_with_count($query);

$pager_options = array(
'mode'       => UT::$_CONFIG['pagination_mode'],   // Sliding or Jumping mode. See below.
'perPage'    => UT::$_CONFIG['per_page'],   // Total rows to show per page
'delta'      => UT::$_CONFIG['page_delta'],   // See below
'totalItems' => $result['total'],
);
 
/* Initialize the Pager class with the above options */
$pager = Pager::factory($pager_options);


$smarty->assign("results",$result['rows']);
$smarty->assign("links",$pager->links);
$smarty->assign("total",$result['total']);

$smarty->display('search.tpl');

?>







