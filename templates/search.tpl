<html>
<head>
    <title>
        {$title}
    </title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>


<body>

<div class="container">
    
    
<div class="row">
<div class="col-sm-12">



<div class="row">
<form method="get">
<fieldset>     
        <legend>Filter Blog Feeds By</legend>
                    
        <div class="panel panel-default">
            <div class="panel-body">
                           
                        <div class="col-lg-6">
                            <div class="input-group">
                             <span class="input-group-addon">Date (YYYY-MM-DD)</span>
                              <input type="date" class="form-control" placeholder="Blog created Date" name="blog_date" value="{ if isset($data.blog_date) }{$data.blog_date}{/if}">
                               <span class="input-group-addon"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="input-group">
                              <span class="input-group-addon">Username</span>
                              <input type="text" class="form-control" placeholder="Username like" name='username' value="{ if isset($data.username) }{$data.username}{/if}">
                               <span class="input-group-addon"></span>
                            </div>
                          </div>
                        
                          <br><br>
                        <div class="col-lg-6">
                            <div class="input-group">
                             <span class="input-group-addon">Server</span>
                              <input type="number" class="form-control" placeholder="Server Id" name="server" value="{ if isset($data.server) }{$data.server}{/if}">
                               <span class="input-group-addon"></span>
                            </div>
                          </div>
                          
                          <div class="col-lg-6">
                            <div class="input-group">
                              <span class="input-group-addon">Entry</span>
                              <input type="number" class="form-control" placeholder="Blog Entry" name='entry' value="{ if isset($data.entry) }{$data.entry}{/if}">
                               <span class="input-group-addon"></span>
                            </div>
                          </div>

                        <br><br><br>

                       
                        
                        <input type="submit" name="search" class="btn btn-primary btn-block" >
                          
            </div>
        </div>

 </fieldset> 
</form>

            
            {$links}
        </div>


      <hr>

{foreach from=$results item=blog}

      <div class="row">
        <div class="col-sm-12">
            <h4> {$blog.title}<span class="label label-default label-arrow label-arrow-left">{$blog.blog_date}</span></h4>  
            <hr>
        </div>
        <div class="col-sm-10"><p> {$blog.description}</p>
          <div class="pull-right"><span class="label label-warning">{$blog.username}</span></div>
          <p> <button class="btn btn-default"><a href='{$blog.link}'>{$blog.link}</a></button></p></div>
      </div>
      <hr>

{/foreach}

    
      <div class="row">
        {$links}
      </div>
      <hr>
    </div>
</div>
</body>
</html>