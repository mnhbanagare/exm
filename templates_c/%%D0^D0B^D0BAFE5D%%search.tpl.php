<?php /* Smarty version 2.6.31, created on 2017-12-04 23:21:55
         compiled from search.tpl */ ?>
<html>
<head>
    <title>
        Search for the Post
    </title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <style type="text/css">
        
    
    </style>
</head>


<body>

<div class="container">
    
    
      <div class="row">
        <div class="col-sm-12">



<div class="row">
<form method="get">
<fieldset>     
        <legend>Filter Blog Feeds By</legend>
                    
        <div class="panel panel-default">
            <div class="panel-body">
                           
                        <div class="col-lg-6">
                            <div class="input-group">
                             <span class="input-group-addon">Date</span>
                              <input type="text" class="form-control" placeholder="Blog created Date" name="blog_date" value="<?php if (isset ( $this->_tpl_vars['data']['blog_date'] )): ?><?php echo $this->_tpl_vars['data']['blog_date']; ?>
<?php endif; ?>">
                               <span class="input-group-addon"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="input-group">
                              <span class="input-group-addon">Username</span>
                              <input type="text" class="form-control" placeholder="Username like" name='username' value="<?php if (isset ( $this->_tpl_vars['data']['username'] )): ?><?php echo $this->_tpl_vars['data']['username']; ?>
<?php endif; ?>">
                               <span class="input-group-addon"></span>
                            </div>
                          </div>
                        
                          <br><br>
                        <div class="col-lg-6">
                            <div class="input-group">
                             <span class="input-group-addon">Server</span>
                              <input type="text" class="form-control" placeholder="Server Id" name="server" value="<?php if (isset ( $this->_tpl_vars['data']['server'] )): ?><?php echo $this->_tpl_vars['data']['server']; ?>
<?php endif; ?>">
                               <span class="input-group-addon"></span>
                            </div>
                          </div>
                          
                          <div class="col-lg-6">
                            <div class="input-group">
                              <span class="input-group-addon">Entry</span>
                              <input type="text" class="form-control" placeholder="Blog Entry" name='entry' value="<?php if (isset ( $this->_tpl_vars['data']['entry'] )): ?><?php echo $this->_tpl_vars['data']['entry']; ?>
<?php endif; ?>">
                               <span class="input-group-addon"></span>
                            </div>
                          </div>

                        <br><br><br>

                       
                        
                        <input type="submit" name="search" class="btn btn-primary btn-block" >
                          
            </div>
        </div>

 </fieldset> 
</form>

            
            <?php echo $this->_tpl_vars['links']; ?>

        </div>


      <hr>

<?php $_from = $this->_tpl_vars['results']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['blog']):
?>

      <div class="row">
        <div class="col-sm-12">
            <h4> <?php echo $this->_tpl_vars['blog']['title']; ?>
<span class="label label-default label-arrow label-arrow-left"><?php echo $this->_tpl_vars['blog']['blog_date']; ?>
</span></h4>  
            <hr>
        </div>
        <div class="col-sm-10"><p> <?php echo $this->_tpl_vars['blog']['description']; ?>
</p>
          <div class="pull-right"><span class="label label-warning"><?php echo $this->_tpl_vars['blog']['username']; ?>
</span></div>
          <p> <button class="btn btn-default"><a href='<?php echo $this->_tpl_vars['blog']['link']; ?>
'><?php echo $this->_tpl_vars['blog']['link']; ?>
</a></button></p></div>
      </div>
      <hr>

<?php endforeach; endif; unset($_from); ?>

    
      <div class="row">
        <?php echo $this->_tpl_vars['links']; ?>

      </div>
      <hr>
    </div>
</div>
</body>
</html>