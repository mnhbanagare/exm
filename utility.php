<?php

/*
* Its Simple Utility file which holds all common methods
* 
*/


// load configuration as an array. we can hadle error if config loading failed 
// $_CONFIG = parse_ini_file('config.ini'); 


/**
* 
*Utility class for handing common functions
*/
class UT 
{
    
    
    public static $_CONFIG;
    public static $connection;


    static function init()
    {
        // load configuration as an array. we can hadle error if config loading failed 
        self::$_CONFIG = parse_ini_file('config.ini'); 

        //connect to db
        self::db_connect();
    }


    function __destruct() {
        //close the ocnnection
       //mysqli_close(self::$connection);
    }


    //common method to print for cron/command line
    public function printInfo($data,$newline = true){
        echo $data;
        if($newline)
        {
            echo "\n";
        }
    }

    //common method to print for cron/command line
    public function printRequired($data,$newline = true){
        echo $data;
        if($newline)
        {
            echo "\n";
        }
    }


    public function printException($expection,$message = ''){
        echo $message."\n";
        print_r($expection);
        echo "\n";
    }




    public static function db_connect() 
    {
        //connect to the database
        if(!isset(self::$connection)) {
            
            self::$connection = mysqli_connect(self::$_CONFIG['host'],self::$_CONFIG['username'],self::$_CONFIG['password'],self::$_CONFIG['dbname']);

            /* change character set to utf8 */
            if (!mysqli_set_charset(self::$connection, self::$_CONFIG['character_set'])) 
            {
                printf("Error loading character set utf8: %s\n", mysqli_error(self::$connection));
                throw new Exception("Error loading character set utf8");
            } else 
            {
                //printf("Current character set: %s\n", mysqli_character_set_name(self::$connection));
            }

        }


        // If connection is not successful throw exception
        if(self::$connection === false) {
            throw new Exeption ('Error connecting db. Error no: '.mysqli_connect_error());
        }


        return self::$connection;
    }



    public static function insert($table_name,$data)
    {

        $columns = array_keys($data);
        $values = array_values($data);

        $query = "INSERT INTO $table_name (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')";

       
        // Query the database
        $result = mysqli_query(self::$connection,$query);

        return $result;

    }


    public static function is_param_set($data,$key)
    {
        if(isset($data[$key]) && $data[$key]!= ""){
            return true;
        }else{
            return false;
        }
    }


    public static function select($query)
    {
        

        $res = mysqli_query(self::$connection,$query);

        while ($row = mysqli_fetch_assoc($res)) {
            $rows[] = $row;
        }

        return $rows;

    }


    public static function raw_query($query)
    {
        

        $res = mysqli_query(self::$connection,$query);

        return $res;

    }



    public static function select_with_count($query)
    {
        
        $result = array();
        $rows = array();

        $res = mysqli_query(self::$connection,$query);

        $count = mysqli_query(self::$connection,' SELECT FOUND_ROWS() as total ');

        $count = $row = mysqli_fetch_assoc($count);


        while ($row = mysqli_fetch_assoc($res)) {
            $rows[] = $row;
        }

        $result['rows'] = $rows;
        $result['total'] = $count['total'];

        return $result;

    }



}



//init the utility class
UT::init();



?>